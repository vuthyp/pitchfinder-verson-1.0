package com.kosign.dao;

import com.kosign.model.User;

public interface UserDao {
	User findByUserName(String username);
}
