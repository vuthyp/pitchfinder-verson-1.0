package com.kosign.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.RememberMeAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HelloController {
//	@RequestMapping(value = { "/", "/welcome**" }, method = RequestMethod.GET)
//	public ModelAndView welcomePage() {
//
//		ModelAndView model = new ModelAndView();
//		model.addObject("title", "Spring Security Hello World");
//		model.addObject("message", "This is welcome page!");
//		model.setViewName("home");
//		return model;
//
//	}

	@RequestMapping(value = {"/","/home**"}, method = RequestMethod.GET)
	public ModelAndView adminPage() {
		Authentication user = SecurityContextHolder.getContext().getAuthentication();
		System.out.println(user.getAuthorities()+"ssssssssssssssssssssssssssssss");
		ModelAndView model = new ModelAndView();
		model.setViewName("FieldManagerment/FieldDetail_Field");

		return model;

	}
//
//	@RequestMapping(value = "/dba**", method = RequestMethod.GET)
//	public ModelAndView dbaPage() {
//
//		ModelAndView model = new ModelAndView();
//		model.addObject("title", "Spring Security Hello World");
//		model.addObject("message", "This is protected page - Database Page!");
//		model.setViewName("admin");
//
//		return model;
//
//	}

	/**
	 * This update page is for user login with password only. If user is login
	 * via remember me cookie, send login to ask for password again. To avoid
	 * stolen remember me cookie to update info
	 */
//	@RequestMapping(value = "/admin/update**", method = RequestMethod.GET)
//	public ModelAndView updatePage(HttpServletRequest request) {
//
//		ModelAndView model = new ModelAndView();
//
//		if (isRememberMeAuthenticated()) {
//			// send login for update
//			setRememberMeTargetUrlToSession(request);
//			model.addObject("loginUpdate", true);
//			model.setViewName("/login");
//
//		} else {
//			model.setViewName("update");
//		}
//
//		return model;
//
//	}

	// Spring Security see this :
	@RequestMapping(value ="/login**", method = RequestMethod.GET)
	public ModelAndView login(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout,HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		
		if (error != null) {
			model.addObject("error", "Invalid username and password!");

			// login form for update page
			// if login error, get the targetUrl from session again.
			String targetUrl = getRememberMeTargetUrlFromSession(request);
			System.out.println(targetUrl);
			if (StringUtils.hasText(targetUrl)) {
				model.addObject("targetUrl", targetUrl);
				model.addObject("loginUpdate", true);
			}
		}

		if (logout != null) {
			model.addObject("msg", "You've been logged out successfully.");
		}
		model.setViewName("FieldManagerment/Field_Login");

		return model;

	}

	// for 403 access denied page
	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public ModelAndView accesssDenied() {

		ModelAndView model = new ModelAndView();

		// check if user is login
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			model.addObject("username", userDetail.getUsername());
		}

		model.setViewName("403");
		return model;

	}

	private boolean isRememberMeAuthenticated() {

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication == null) {
			return false;
		}

		return RememberMeAuthenticationToken.class.isAssignableFrom(authentication.getClass());
	}

	/**
	 * save targetURL in session
	 */
	private void setRememberMeTargetUrlToSession(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		if (session != null) {
			session.setAttribute("targetUrl", "/admin/update");
		}
	}

	/**
	 * get targetURL from session
	 */
	private String getRememberMeTargetUrlFromSession(HttpServletRequest request) {
		String targetUrl = "";
		HttpSession session = request.getSession(false);
		if (session != null) {
			targetUrl = session.getAttribute("targetUrl") == null ? "" : session.getAttribute("targetUrl").toString();
		}
		return targetUrl;
	}
	
	
//	public static void main(String args[]){
//		System.out.println(new BCryptPasswordEncoder().encode("admin"));
//	}

}
