<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<!-- Latest compiled and minified CSS -->
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">

<!-- Optional theme -->
<link href="<c:url value="resources/css/bootstrap-theme.min.css" />"
	rel="stylesheet">

<!-- MY CSS -->
<link href="<c:url value="/resources/css/mycss.css" />" rel="stylesheet">
<link
	href='https://fonts.googleapis.com/css?family=Roboto:100,900italic,900,700italic,700,500italic,500,400italic,400,300italic,300,100italic'
	rel='stylesheet' type='text/css'>


<!-- Datepicker -->
<link rel="stylesheet"
	href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet"
	href="<c:url value="/resources/demos/style.css"/>">
<script>
	$(function() {
		$("#datepicker").datepicker();
	});
</script>


<!-- Latest compiled and minified JavaScript -->
<script src="<c:url value="resources/js/bootstrap.min.js"/>"
	type="text/javascript"></script>

<title>FieldDetail_Field</title>
<style>
.mylayout {
	border: 1px solid black;
}
</style>

<!-- MY JS -->
<script src="<c:url value="resources/js/myjs.js"/>"
	type="text/javascript"></script>
<head>
<body>
	<c:url var="logoutUrl" value="/logout" />
	<form action="${logoutUrl}" method="post">
		<input type="submit" value="Log out" /> <input type="hidden"
			name="${_csrf.parameterName}" value="${_csrf.token}" />
	</form>

	<%-- <script>
		function formSubmit() {
			document.getElementById("logoutForm").submit();
		}
	</script>--%>

	<c:if test="${pageContext.request.userPrincipal.name != null}">
		<h2>Welcome : ${pageContext.request.userPrincipal.name}</h2>
	</c:if>
	<div class="container">
		<!-- top nav bar -->
		<div class="col-md-12" id="topnavbar">
			<div class="col-md-5">
				<h1 class="roboto900italic">Pitch Finder</h1>
			</div>
			<div class="col-md-3">
				<h1 class="roboto900italic">Rooy7</h1>
			</div>
			<div class="col-md-4" style="margin-top: 30px">
				<span class="pull-right white"><a href="#">My Team</a> <i
					class="fa fa-user"></i> 
					<c:if test="${pageContext.request.userPrincipal.name != null}">
						${pageContext.request.userPrincipal.name}
					</c:if> </span>
			</div>
		</div>
		<!-- end top nav bar -->

		<!-- header -->
		<div class="col-md-12" id="header"
			style="padding: 10px 30px; margin-bottom: 10px; background: #373535">
			<ul class="my-nav roboto100">
				<li role="presentation"><a href="FieldDetail_Field.html">Field</a></li>
				<li role="presentation"><a href="FieldDetail_Booking.html">Booking</a></li>
				<li role="presentation"><a href="FieldDetail_Check_Out.html">Check
						out</a></li>
				<li role="presentation"><a
					href="FieldDetail_Setting_Field.html">Setting</a></li>
				<li role="presentation"><a
					href="FieldDetail_PlayGound_Profile.html">Play Ground Profile</a></li>
			</ul>
		</div>
		<!-- end header -->

		<!-- body -->
		<div class="col-md-12" id="body">
			<div class="row" style="margin: 0px; padding: 10px 0px">
				<div class="col-md-2">
					<select class="form-control">
						<option>Field type...</option>
						<option>Big</option>
						<option>Small</option>
					</select>
				</div>
				<div class="col-md-2" style="padding: 0px">
					<input type="text" class="form-control" id="datepicker"
						value="10/07/2015">
				</div>
			</div>
			<div class="col-md-2">
				<table class="table table-bordered" style="background: white">
					<tr>
						<th>Field</th>
					</tr>
					<tr>
						<th>A</th>
					</tr>
					<tr>
						<th>B</th>
					</tr>
					<tr>
						<th>C</th>
					</tr>
					<tr>
						<th>D</th>
					</tr>
					<tr>
						<th>E</th>
					</tr>
					<tr>
						<th>E</th>
					</tr>
					<tr>
						<th>E</th>
					</tr>
					<tr>
						<th>E</th>
					</tr>
				</table>
			</div>
			<div class="col-md-7" style="overflow: auto; padding: 0px">
				<table class="table table-bordered" style="background: white">
					<tr>
						<th>10:00am</th>
						<th>11:00am</th>
						<th>12:00AM</th>
						<th>1:00pm</th>
						<th>2:00pm</th>
						<th>3:00pm</th>
						<th>4:00pm</th>
						<th>5:00pm</th>
						<th>6:00pm</th>
						<th>7:00pm</th>
						<th>8:00pm</th>
						<th>9:00pm</th>
					</tr>
					<tr>
						<td>0</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>0</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>0</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>0</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>0</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>0</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>0</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>0</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</table>
			</div>
			<div class="col-md-3">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Item One</h3>
					</div>
					<div class="panel-body">
						<select class="form-control">
							<option>Select Field...</option>
							<option>Field A</option>
							<option>Field B</option>
						</select> <br> <select class="form-control">
							<option>Start Time...</option>
							<option>07:00pm</option>
							<option>08:00pm</option>
						</select> <br> <select class="form-control">
							<option>Stop Time...</option>
							<option>07:00pm</option>
							<option>08:00pm</option>
						</select> <br> <label>Time : 6:00pm - 8:00pm</label><br> <label>Duration
							: 2h</label><br> <label>Price : 12$</label> <br>
						<button class="btn btn-default">Book</button>
						<button class="btn btn-default">Check In</button>
					</div>
				</div>
			</div>
		</div>
		<!-- end body -->
	</div>
</body>
</html>
