<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<link href="<c:url value="/resources/css/main.css" />" rel="stylesheet">

<!-- Latest compiled and minified CSS -->
<link href="<c:url value="/resources/css/bootstrap.min.css" />"
	rel="stylesheet">

<!-- Optional theme -->
<!-- <link rel="stylesheet" href="css/bootstrap-theme.min.css"> -->
<link href="<c:url value="resources/css/bootstrap-theme.min.css" />"
	rel="stylesheet">

<!-- MY CSS -->
<!-- <link rel="stylesheet" href="css/mycss.css"> -->
<%-- <link href="<c:url value="/resources/css/mycss.css" />" rel="stylesheet"> --%>
<link href="<c:url value="/resources/css/mycss.css" />" rel="stylesheet">
<link
	href='https://fonts.googleapis.com/css?family=Roboto:100,900italic,900,700italic,700,500italic,500,400italic,400,300italic,300,100italic'
	rel='stylesheet' type='text/css'>


<!-- Datepicker -->
<link rel="stylesheet"
	href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
	$(function() {
		$("#datepicker").datepicker();
	});
</script>


<!-- Latest compiled and minified JavaScript -->
<!-- <script src="js/bootstrap.min.js"></script> -->
<script src="<c:url value="resources/js/bootstrap.min.js"/>"
	type="text/javascript"></script>
<%-- <link href="<c:url value="/resources/js/bootstrap.min.js" />" rel="stylesheet"> --%>

<title>Field_Login</title>
<style>
.mylayout {
	border: 1px solid black;
}

body {
	/* 		background: url("photo/simple-clean-blue-pattern.jpg") no-repeat; */
	background: url("<c:url value="/ resources/ photo/ simple-clean-blue-pattern.jpg
		" />") no-repeat;
}

h1, h2, h3 {
	margin: 0px !important;
}
</style>

<!-- MY JS -->
<!-- <script src="js/myjs.js"></script> -->
<script src="<c:url value="resources/js/myjs.js"/>"
	type="text/javascript"></script>
<head>
<body>
	<div class="container" style="background: none">
		<!-- Login Form -->
		<!-- <div id="login-wrapper">
			<div id="login-header">
				<h1 class="roboto900italic">Pitch Finder</h1>
				<h2 class="white roboto100" style="color: white">
					<i>Admin</i>
				</h2>
			</div>
			<div id="login-body">
				<input type="text" class="login-textbox" placeholder="Username...">
				<input type="password" class="login-textbox"
					placeholder="Password..."> <span class="pull-left"
					style="color: #807b7b"> <input type="checkbox"> <small>Remember
						me</small>
				</span> <span class="pull-right" style="color: #807b7b"> <small>Don't
						have account yet ?<br>
					<a href="Field_Register.html">Register here !</a>
				</small>
				</span>
			</div>
			<div id="login-footer">
				<button id="login-loginBtn">
					<h3 class="roboto900italic" style="color: #900404">
						Login
						<h3>
				</button>
			</div>
		</div> -->


		<div id="login-box">

			<h2>Login with Username and Password</h2>

			<c:if test="${not empty error}">
				<div class="error">${error}</div>
			</c:if>
			<c:if test="${not empty msg}">
				<div class="msg">${msg}</div>
			</c:if>
			<%-- <c:url value='j_spring_security_check' var="loginURL"/> --%>
			<form name='loginForm' action="login" method='POST'>

				<table>
					<tr>
						<td>User:</td>
						<td><input type='text' name='username' value=''></td>
					</tr>
					<tr>
						<td>Password:</td>
						<td><input type='password' name='password' /></td>
					</tr>

					<!-- if this is login for update, ignore remember me check -->
					<c:if test="${empty loginUpdate}">
						<tr>
							<td></td>
							<td>Remember Me: <input type="checkbox" name="remember-me" /></td>
						</tr>
					</c:if>


					<tr>
						<td colspan='2'><input name="submit" type="submit"
							value="submit" /></td>
					</tr>
				</table>
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
			</form>
		</div>
		<!-- End Login Form -->
	</div>
</body>
</html>
